package edu.ab.dogcircus.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import edu.ab.dogcircus.data.ConnectionUtil;

public class DogCircus {

    private static final Integer MINIMUM_OPTION = 1;
    private static final Integer MAXIMUM_OPTION = 8;
    private static Scanner scanner = new Scanner(System.in);

    private static void getMenu() {

 System.out.println("*** DOG CIRCUS ***");
 System.out.println("1. Add Dog");
 System.out.println("2. Edit Dog");
 System.out.println("3. View Dog Details");
 System.out.println("4. Delete Dog");
 System.out.println("5. List All Dogs");
 System.out.println("6. Find Dogs by Owner");
 System.out.println("7. Find Dogs by ID");
 System.out.println("8. Exit");

    }

    private static void addDog() {
 System.out.println("== Add Dog ==");
 String dogID;
 String dogName;
 String ownerID;

 scanner.nextLine();
 System.out.print("DOG ID => ");
 dogID = scanner.nextLine();

 System.out.print("DOG NAME => ");
 dogName = scanner.nextLine();

 System.out.print("OWNER ID => ");
 ownerID = scanner.nextLine();

 String addDogStmt = "INSERT INTO dog VALUES('" + dogID + "','" + dogName
  + "'," + ownerID + ")";

 ConnectionUtil.executeQuery(addDogStmt);

    }
    
    
    private static void editDog() {

 System.out.println("== Edit Dog ==");
 String dogName;
 String dogID;
 String ownerID;
 String commit;

 scanner.nextLine();
 System.out.print("DOG ID => ");
 dogID = scanner.nextLine();

 String lookupDogQuery = "SELECT * FROM dog WHERE isbn = '" + dogID
  + "'";

 Connection connection = ConnectionUtil.getConnection();
 Statement statement = ConnectionUtil.getStatement(connection);
 try {
     ResultSet resultSet = statement.executeQuery(lookupDogQuery);

     resultSet.next();
     String foundDogID = resultSet.getString("id");
     String foundDogName = resultSet.getString("name");
     String foundOwnerID = resultSet.getString("owner_id");

     System.out.println("DOG ID: " + foundDogID);
     System.out.println("DOG NAME: " + foundDogName);
     System.out.println("OWNER ID: " + foundOwnerID + "\n");

     System.out.print("NEW NAME => ");
     dogName = scanner.nextLine();

     System.out.print("NEW OWNER ID => ");
     ownerID = scanner.nextLine();

     System.out.println("\n= Update Info = \nDOG ID => " + dogID
      + "\nNAME =>   " + dogName + "  -->  " + dogName
      + "\nOWNER ID =>   " + foundOwnerID + "  -->  "
      + ownerID
      + "\n\nCommit these changes to Database? (y/n)");
     commit = scanner.nextLine();

     if (commit.equals("y")) {

  String editDogStmt = "UPDATE dog SET title = '" + dogName
   + "', owner_id = '" + ownerID
   + "' WHERE id = '" + dogID + "'";

  ConnectionUtil.executeQuery(editDogStmt);

  System.out.println("Change to Database Successful\n");

     } else if (commit.equals("n")) {
  System.out
   .println("Changes not committed. Returning to Main Menu");
     } else {
  System.out
   .println("[ERROR] Invalid choice selected! Returning to Main Menu");
     }

 } catch (SQLException e) {
     // e.printStackTrace();
     System.out.println("Unable to locate dog with ID => " + dogID);
 } finally {

     ConnectionUtil.closeConnection(connection);

 }
    }
    
    
    
    private static void viewDog() {

 System.out.println("== View Dog ==");
 String dogID;

 scanner.nextLine();
 System.out.print("DOG ID => ");
 dogID = scanner.nextLine();

 String lookupDogQuery = "SELECT * FROM dog WHERE id = '" + dogID
  + "'";

 Connection connection = ConnectionUtil.getConnection();
 Statement statement = ConnectionUtil.getStatement(connection);
 try {
     ResultSet resultSet = statement.executeQuery(lookupDogQuery);

     resultSet.next();
     String foundDogID = resultSet.getString("id");
     String foundName = resultSet.getString("name");
     String foundOwnerID = resultSet.getString("owner_id");

     System.out.println("Dog ID: " + foundDogID);
     System.out.println("Name: " + foundName);
     System.out.println("Owner ID: " + foundOwnerID);

 } catch (SQLException e) {
     // e.printStackTrace();
     System.out.println("Unable to locate dog with ID => " + dogID);
 } finally {

     ConnectionUtil.closeConnection(connection);

 }

    }
    
    
    
    private static void deleteDog() {

 System.out.println("== Delete Dog ==");
 String dogID;

 scanner.nextLine();
 System.out.print("DOG ID => ");
 dogID = scanner.nextLine();

 String deleteDogStmt = "DELETE FROM dog WHERE id = '" + dogID + "'";

 ConnectionUtil.executeQuery(deleteDogStmt);

    }
    
    
    
    private static void showAllDogs() {

 System.out.println("== All Dogs ==");

 String lookupDogQuery = "SELECT * FROM dog";

 Connection connection = ConnectionUtil.getConnection();
 Statement statement = ConnectionUtil.getStatement(connection);
 try {
     ResultSet resultSet = statement.executeQuery(lookupDogQuery);

     while (resultSet.next()) {
  String foundDogID = resultSet.getString("id");
  String foundName = resultSet.getString("name");
  String foundOwnerID = resultSet
   .getString("owner_id");

  System.out.println("Dog ID: " + foundDogID);
  System.out.println("Name: " + foundName);
  System.out.println("Owner ID: " + foundOwnerID);
     }
 } catch (SQLException e) {
     e.printStackTrace();
     System.out.println("Unable to locate dogs!");
 } finally {

     ConnectionUtil.closeConnection(connection);

 }

    }
    
    
    
    private static void findDogByOwner() {

 System.out.println("== View Dog by Owner ==");
 String ownerID;

 scanner.nextLine();
 System.out.print("OWNER ID => ");
 ownerID = scanner.nextLine();

 String lookupDogQuery = "SELECT * FROM dog WHERE owner_id = " + ownerID;

 Connection connection = ConnectionUtil.getConnection();
 Statement statement = ConnectionUtil.getStatement(connection);
 try {
     ResultSet resultSet = statement.executeQuery(lookupDogQuery);

     while (resultSet.next()) {
     String foundDogID = resultSet.getString("id");
     String foundName = resultSet.getString("name");
     String foundOwnerID = resultSet.getString("owner_id");

     System.out.println("ISBN: " + foundDogID);
     System.out.println("Title: " + foundName);
     System.out.println("Year Published: " + foundOwnerID);
     }
 } catch (SQLException e) {
     // e.printStackTrace();
     System.out.println("Unable to locate dog with OwnerID => " + ownerID);
 } finally {

     ConnectionUtil.closeConnection(connection);

 }

    }
    
    
    
    private static void findDogByID() {

 System.out.println("== View Dog by ID ==");
 String dogID;

 scanner.nextLine();
 System.out.print("DOG ID => ");
 dogID = scanner.nextLine();

 String lookupDogQuery = "SELECT * FROM dog WHERE id = " + dogID;

 Connection connection = ConnectionUtil.getConnection();
 Statement statement = ConnectionUtil.getStatement(connection);
 try {
     ResultSet resultSet = statement.executeQuery(lookupDogQuery);

     while (resultSet.next()) {
     String foundDogID = resultSet.getString("id");
     String foundName = resultSet.getString("name");
     String foundOwnerID = resultSet.getString("owner_id");

     System.out.println("ISBN: " + foundDogID);
     System.out.println("Title: " + foundName);
     System.out.println("Year Published: " + foundOwnerID);
     }
 } catch (SQLException e) {
     // e.printStackTrace();
     System.out.println("Unable to locate dog with DogID => " + dogID);
 } finally {

     ConnectionUtil.closeConnection(connection);

 }

    }

    
    
    
    private static Integer getOption() {

 Boolean validOption = false;
 Integer option = 0;
 do {
     System.out.print("Select an option ==> ");
     try {
  option = scanner.nextInt();

  if (option >= MINIMUM_OPTION && option <= MAXIMUM_OPTION) {
      validOption = true;
  } else {
      System.out.println("[ERROR] Selection must be between "
       + MINIMUM_OPTION + " and " + MAXIMUM_OPTION);
  }

     } catch (InputMismatchException e) {
  System.out.println("[ERROR] Input must be an integer!");
  scanner.next();
     }

 } while (!validOption);
 return option;

    }
    
    
    public static void main(String... args) {

 Integer menuOption = 0;
 do {

     getMenu();
     menuOption = getOption();
     switch (menuOption) {
     case 1:
  addDog();
  break;
     case 2:
  editDog();
  break;
     case 3:
  viewDog();
  break;
     case 4:
  deleteDog();
  break;
     case 5:
  showAllDogs();
  break;
     case 6:
  findDogByOwner();
  break;
     case 7:
  findDogByID();
  break;
     case 8:
  System.exit(0);
  break;

     default:
  System.out.println("[ERROR] An invalid option was selected!");
     }

 } while (menuOption != MAXIMUM_OPTION);

 main();
    }

    
}
